package analysis.staticc.jdt.astvisitors;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import analysis.staticc.jdt.xmlwriter.ClassDiagramWriter;
import analysis.staticc.jdt.xmlwriter.ClassDiagramXMLWriter;

/**
 * For each relevant {@link ASTNode}, gather the necessary information
 * in order to construct a class diagram. 
 *  
 * @authors BarryMcAuley, AgustinHales
 *
 */
public class ClassDiagramASTVisitor extends ASTVisitor{

	private static ClassDiagramASTVisitor instance = null ;
	private ClassDiagramWriter writer = ClassDiagramXMLWriter.getInstance() ;

	private ClassDiagramASTVisitor() {
		super() ;
	}

	/**
	 * Required for Singelton instantiation
	 */
	public static synchronized ClassDiagramASTVisitor getInstance() {
		if (instance == null) 
			instance = new ClassDiagramASTVisitor() ;
		return instance ;
	}

	/**
	 * Visiting TypeDeclaration and writing;
	 * class_name, modifiers, class_type, 
	 * generalisation and relasation to XML
	 */
	@Override
	public boolean visit(TypeDeclaration typeDecl) {		
		writer.insertClassElement() ;
		writer.attachAttributeToElement("class_name", typeDecl.getName().toString()) ;
		writer.attachAttributeToElement("modifiers", typeDecl.modifiers().toString()) ;
		if (typeDecl.isInterface())
			writer.attachAttributeToElement("class_type", "Interface") ;
		else
			writer.attachAttributeToElement("class_type", "Class");
		
		// Extends...
		if (typeDecl.getSuperclassType() != null) {
			writer.attachRelationElement() ;
			writer.attachAttributeToElement("generalisation", typeDecl.getSuperclassType().toString()) ;
		}
		// Implements...
		if ( !typeDecl.superInterfaceTypes().isEmpty()) {
			writer.attachRelationElement() ;
			writer.attachAttributeToElement("realisation", typeDecl.superInterfaceTypes().toString()) ;
		}
		return true ;
	}

	/**
	 * Visiting MethodDeclaration and writing;
	 * modifiers, name and return_type to XML
	 */
	@Override
	public boolean visit(MethodDeclaration node) {
		if( !node.isConstructor() ) {
			writer.attachMethodDeclaration() ;
			writer.attachAttributeToElement("modifiers", node.modifiers().toString() ) ;
			writer.attachAttributeToElement("name", node.getName().toString() ) ;
			writer.attachAttributeToElement("return_type", node.getReturnType2().toString() ) ;

			@SuppressWarnings("unchecked")
			List<SingleVariableDeclaration> paramaters = node.parameters() ;
			for(Integer i = 0; i < paramaters.size(); i++) {
				SingleVariableDeclaration parameter = paramaters.get(i) ;
				writer.attachRelationElement() ;
				writer.attachAttributeToElement("dependency", parameter.getType().toString() ) ;
			}

			if (node.getBody() != null)
				node.getBody().accept(this) ;
		}
		return false; 
	}

	/**
	 * Visiting FieldDeclaration and writing;
	 * modifiers, type and names to XML
	 */
	@Override
	public boolean visit(FieldDeclaration fd) {
		writer.attachFieldDeclaration() ;
		writer.attachAttributeToElement("modifiers", fd.modifiers().toString() ) ;
		writer.attachAttributeToElement("type", fd.getType().toString() ) ;
		writer.attachAttributeToElement("names", fd.fragments().toString() ) ;
		return false;
	}

	/**
	 * Visiting ClassInstanceCreation and writing;
	 * association to XML
	 */
	public boolean visit(ClassInstanceCreation classInstanceCreation) {
		writer.attachRelationElement() ;
		writer.attachAttributeToElement("association", classInstanceCreation.getType().toString() ) ;
		return false ;
	}

}
