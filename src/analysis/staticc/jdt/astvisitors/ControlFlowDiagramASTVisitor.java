package analysis.staticc.jdt.astvisitors;

import java.util.Stack;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

import analysis.staticc.jdt.xmlwriter.ControlFlowDiagramXMLWriter;
import analysis.staticc.jdt.xmlwriter.ControlFlowWriter;

public class ControlFlowDiagramASTVisitor extends ASTVisitor {

	private static ControlFlowDiagramASTVisitor instance = null ;
	private ControlFlowWriter writer = ControlFlowDiagramXMLWriter.getInstance() ;

	// Required to keep track of parents in statements
	private Stack<ASTNode> previousParents ;

	private ControlFlowDiagramASTVisitor(){
		super();
		previousParents = new Stack<ASTNode>() ;
	}

	/**
	 * Required for Singelton instantiation
	 */
	public static synchronized ControlFlowDiagramASTVisitor getInstance() {
		if( instance == null )
			instance = new ControlFlowDiagramASTVisitor() ;
		return instance ;
	}

	@Override
	public boolean visit(TypeDeclaration typeDecl) {
		if (!typeDecl.isInterface()) {
			writer.attachClassElement() ;
			writer.attachAttributeToElement("class_name", typeDecl.getName().toString()) ;
			previousParents.push(typeDecl) ;
			return true ;
		}
		return false ;
	}

	@Override
	public boolean visit(MethodDeclaration methodDeclaration)  {
		checkParent(methodDeclaration) ;
		writer.attachMethodElement() ;
		writer.attachAttributeToElement("name", methodDeclaration.getName().toString()) ;
		if (methodDeclaration.getReturnType2() != null)
			writer.attachAttributeToElement("return-type", methodDeclaration.getReturnType2().toString()) ;
		if (methodDeclaration.getBody() != null)  {
			previousParents.push(methodDeclaration.getBody()) ;
			methodDeclaration.getBody().accept(this) ;
		}
		return false;
	}

	@Override
	public boolean visit(IfStatement ifStatement) {
		checkParent(ifStatement) ;
		writer.attachIfStatement() ;
		writer.attachAttributeToElement("expression", ifStatement.getExpression().toString()) ;
		previousParents.push(ifStatement) ;
		previousParents.push(ifStatement.getThenStatement()) ;
		ifStatement.getThenStatement().accept(this) ; 		// Visit inner blocks...

		if (ifStatement.getElseStatement() != null) {
			writer.attachElseStatement() ;
			previousParents.push(ifStatement.getElseStatement()) ;
			ifStatement.getElseStatement().accept(this) ;
		}

		return false ;
	}

	@Override
	public boolean visit(ForStatement forStatement) {
		checkParent(forStatement) ;
		writer.attachForStatement() ;
		writer.attachAttributeToElement("expression", forStatement.getExpression().toString()) ;
		previousParents.push(forStatement.getBody()) ;
		forStatement.getBody().accept(this) ;
		return false ;
	}

	@Override
	public boolean visit(DoStatement doStatement) {
		checkParent(doStatement) ;
		writer.attachDoStatement() ;
		writer.attachAttributeToElement("expression", doStatement.getExpression().toString()) ;
		previousParents.push(doStatement.getBody()) ;
		doStatement.getBody().accept(this) ;
		return false ;
	}

	@Override
	public boolean visit(WhileStatement whileStatement) {
		checkParent(whileStatement) ;
		writer.attachWhileStatement() ;
		writer.attachAttributeToElement("expression", whileStatement.getExpression().toString()) ;
		previousParents.push(whileStatement.getBody()) ;
		whileStatement.getBody().accept(this) ;
		return false ;
	}

	@Override
	public boolean visit(SwitchStatement switchStatement) {
		checkParent(switchStatement) ;
		writer.attachSwitchStatement() ;
		writer.attachAttributeToElement("expression", switchStatement.getExpression().toString()) ;
		previousParents.push(switchStatement) ;
		return true ;
	}

	@Override
	public boolean visit(SwitchCase switchCase) {
		checkParent(switchCase) ;
		writer.attachSwitchCaseStatement() ;
		if (switchCase.getExpression() != null)
			writer.attachAttributeToElement("expression", switchCase.getExpression().toString()) ;
		previousParents.push(switchCase) ;
		return true ;
	}
	
	@Override
	public boolean visit(FieldDeclaration fieldDeclaration) {
		return false;
	}
	
	@Override
	public boolean visit(MethodInvocation methodInvocation) {
		checkParent(methodInvocation.getParent());
		writer.attachGenericStatement();
		writer.attachAttributeToElement("value", methodInvocation.toString());
		previousParents.push(methodInvocation);
		return false;
	}

	@Override
	public boolean visit(TryStatement tryStatement) {
		// This is a statement as it does not affect the control
		// We will assume that exceptions will NOT occur during normal execution.
		checkParent(tryStatement) ;
		writer.attachTryStatement() ;
		previousParents.push(tryStatement.getBody()) ;
		tryStatement.getBody().accept(this) ;
		return false ;
	}

	@Override
	public boolean visit(VariableDeclarationStatement varDecl) {
		checkParent(varDecl) ;
		writer.attachGenericStatement();
		writer.attachAttributeToElement("value", varDecl.toString()) ;
		previousParents.push(varDecl) ;
		return false ;
	}

	@Override
	public boolean visit(Assignment assignemnt) {
		checkParent(assignemnt.getParent()) ;
		writer.attachGenericStatement() ;
		writer.attachAttributeToElement("value", assignemnt.toString()) ;
		previousParents.push(assignemnt) ;
		return false ; 
	}

	@Override
	public boolean visit(PostfixExpression postFixExpression) {
		checkParent(postFixExpression) ;
		writer.attachGenericStatement() ;
		writer.attachAttributeToElement("value", postFixExpression.toString());
		previousParents.push(postFixExpression) ;
		return false ;
	}

	@Override
	public boolean visit(PrefixExpression preFixExpression) {
		checkParent(preFixExpression) ;
		writer.attachGenericStatement() ;
		writer.attachAttributeToElement("value", preFixExpression.toString());
		previousParents.push(preFixExpression) ;
		return false ;
	}

	@Override
	public boolean visit(BreakStatement breakStatement) {
		checkParent(breakStatement) ;
		writer.attachGenericStatement();
		writer.attachAttributeToElement("value", breakStatement.toString()) ;
		previousParents.push(breakStatement) ;
		return false ;
	}

	@Override
	public boolean visit(ContinueStatement continueStatement) {
		checkParent(continueStatement) ;
		writer.attachGenericStatement();
		writer.attachAttributeToElement("value", continueStatement.toString()) ;
		previousParents.push(continueStatement) ;
		return false ;
	}

	@Override
	public boolean visit(ReturnStatement returnStatement) {
		checkParent(returnStatement) ;
		writer.attachGenericStatement();
		writer.attachAttributeToElement("value", returnStatement.toString()) ;
		previousParents.push(returnStatement) ;
		return false ;
	}

	private void checkParent (ASTNode currentElement) {
		while( !previousParents.peek().equals(currentElement.getParent()) ) {
			previousParents.pop() ;
			writer.popStack() ; // The writer maintains a stack
		}
	}
}
