package analysis.staticc.jdt.xmlwriter;

import java.io.File;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * XML implementation of the writer interface
 * @author BarryMcAuley
 *
 */
public class ControlFlowDiagramXMLWriter implements ControlFlowWriter {

	private static ControlFlowDiagramXMLWriter instance = null ;
	private Document document ;
	private Stack<Element> upperElements ;
	private Element rootElement, currentElement ;

	private ControlFlowDiagramXMLWriter() throws ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance() ;
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder() ;
		document = documentBuilder.newDocument() ;
		rootElement = document.createElement("CLASSES") ;
		document.appendChild(rootElement) ;
		upperElements = new Stack<Element>() ;
		upperElements.push(rootElement) ;
	}

	public static synchronized ControlFlowDiagramXMLWriter getInstance() {
		if (instance == null) {
			try {
				instance = new ControlFlowDiagramXMLWriter() ;
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
		return instance ;
	}
	
	@Override
	public void attachClassElement() {
		currentElement = document.createElement("CLASS") ;
		rootElement.appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}
	
	@Override
	public void attachMethodElement() {
		currentElement = document.createElement("METHOD") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}
	
	@Override
	public void attachGenericStatement() {
		currentElement = document.createElement("STATEMENT") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}
		
	@Override
	public void attachIfStatement() {
		currentElement = document.createElement("IF") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
		upperElements.push(currentElement) ;
	}
	
	@Override
	public void attachElseStatement() {
		currentElement = document.createElement("ELSE") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachForStatement() {
		currentElement = document.createElement("FOR") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachWhileStatement() {
		currentElement = document.createElement("WHILE") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachDoStatement() {
		currentElement = document.createElement("DO") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachSwitchStatement() {
		currentElement = document.createElement("SWITCH") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}
	
	@Override
	public void attachSwitchCaseStatement() {
		currentElement = document.createElement("SWITCH-CASE") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachTryStatement() {
		currentElement = document.createElement("TRY") ;
		upperElements.peek().appendChild(currentElement) ;
		upperElements.push(currentElement) ;
	}

	@Override
	public void attachAttributeToElement(String id, String value) {
		Attr attributeToAdd = document.createAttribute(id) ;
		attributeToAdd.setValue(value) ;
		currentElement.setAttributeNode(attributeToAdd) ;
	}

	@Override
	public void buildFile(String fileName) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance() ;
		Transformer transformer = transformerFactory.newTransformer() ;
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		DOMSource source = new DOMSource(document) ;
		StreamResult result = new StreamResult(new File(fileName)) ;
		transformer.transform(source, result);
	}
	
	@Override
	public void popStack() {
		upperElements.pop() ;
	}

}
