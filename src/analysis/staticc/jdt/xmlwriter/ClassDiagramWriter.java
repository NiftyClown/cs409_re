package analysis.staticc.jdt.xmlwriter;


public interface ClassDiagramWriter extends Writer {
 
	/**
	 * Add a new class
	 */
	public void insertClassElement() ;
		
	/**
	 * Add a new field declaration to the file.
	 */
	public void attachFieldDeclaration() ;
	
	/**
	 * Add a new method declaration to the file.
	 */
	public void attachMethodDeclaration() ;
	
	/**
	 * Attach a new relation element to the file.
	 */
	public void attachRelationElement() ;

}
