package analysis.staticc.jdt.xmlwriter;

public interface ControlFlowWriter extends Writer {

	/**
	 * Add an element.
	 */
	public void attachClassElement() ;
	
	public void attachMethodElement() ;
	
	public void attachGenericStatement() ;
		
	public void attachIfStatement() ;
	
	public void attachElseStatement() ;
	
	public void attachForStatement() ;
	
	public void attachWhileStatement() ;
	
	public void attachDoStatement() ;
	
	public void attachSwitchStatement() ;
	
	public void attachSwitchCaseStatement() ;
	
	public void attachTryStatement() ;
	
	/**
	 * Required for keeping track of parent elements when writing
	 */
	public void popStack() ;

}
