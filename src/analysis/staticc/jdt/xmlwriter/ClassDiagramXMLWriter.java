package analysis.staticc.jdt.xmlwriter;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * XML implementation of the writer interface
 * @author BarryMcAuley
 *
 */
public class ClassDiagramXMLWriter implements ClassDiagramWriter {

	private static ClassDiagramXMLWriter instance = null ;
	private Document document ;
	private Element rootElement, classElement, relationElement, fieldElements, methodElements, currentElement ;

	private ClassDiagramXMLWriter() throws ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance() ;
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder() ;
		document = documentBuilder.newDocument() ;
		rootElement = document.createElement("classes") ;
		document.appendChild(rootElement) ;
	}

	public static synchronized ClassDiagramXMLWriter getInstance() {
		if (instance == null) {
			try {
				instance = new ClassDiagramXMLWriter() ;
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
		return instance ;
	}

	@Override
	public void insertClassElement() {
		classElement = document.createElement("class") ;
		relationElement = document.createElement("relations") ;
		fieldElements = document.createElement("fields") ;
		methodElements = document.createElement("methods") ;

		rootElement.appendChild(classElement) ;
		classElement.appendChild(relationElement) ;
		classElement.appendChild(fieldElements) ;
		classElement.appendChild(methodElements) ;
		currentElement = classElement ;
	}

	@Override
	public void attachFieldDeclaration() {
		currentElement = document.createElement("field") ;
		fieldElements.appendChild(currentElement) ;
	}

	@Override
	public void attachMethodDeclaration() {
		currentElement = document.createElement("method") ;
		methodElements.appendChild(currentElement) ;
	}

	@Override
	public void attachRelationElement() {
		currentElement = document.createElement("relation") ;
		relationElement.appendChild(currentElement) ;
	}

	@Override
	public void attachAttributeToElement(String id, String value) {
		Attr attributeToAdd = document.createAttribute(id) ;
		attributeToAdd.setValue(value) ;
		currentElement.setAttributeNode(attributeToAdd) ;
	}

	@Override
	public void buildFile(String fileName) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance() ;
		Transformer transformer = transformerFactory.newTransformer() ;
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		DOMSource source = new DOMSource(document) ;
		StreamResult result = new StreamResult(new File(fileName)) ;
		transformer.transform(source, result);
	}

}
