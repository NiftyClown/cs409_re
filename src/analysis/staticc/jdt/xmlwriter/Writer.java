package analysis.staticc.jdt.xmlwriter;

import javax.xml.transform.TransformerException;

public interface Writer {
	
	/**
	 * Attach details to an element.
	 * 
	 * @param id
	 * @param value
	 */
	public void attachAttributeToElement(String id, String value) ;
	
	/**
	 * Construct the file with the given information
	 * 
	 * @param fileName
	 * @throws TransformerException
	 */
	public void buildFile(String fileName) throws TransformerException ;

}
