package analysis.staticc.jdt.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;

import analysis.staticc.jdt.astvisitors.ClassDiagramASTVisitor;
import analysis.staticc.jdt.astvisitors.ControlFlowDiagramASTVisitor;
import analysis.staticc.jdt.xmlwriter.ClassDiagramXMLWriter;
import analysis.staticc.jdt.xmlwriter.ControlFlowDiagramXMLWriter;

import com.strobel.decompiler.Decompiler;
import com.strobel.decompiler.PlainTextOutput;


public class Client {

	private JarFile jar;

	public Client() {
	}

	public void readSourceForControlFlow(String source) throws IOException {
		List<File> files = new LinkedList<File>() ;
		Files.walk(Paths.get(source)).forEach(filePath -> {
			if (Files.isRegularFile(filePath)) {
				File f = new File(filePath.toString());
				files.add(f) ;
			}
		} );

		for (File javaFile : files) {
			String javaSource = FileUtils.readFileToString(javaFile) ;
			ASTParser javaParser = ASTParser.newParser(AST.JLS3) ;
			javaParser.setSource(javaSource.toCharArray()) ;
			javaParser.setKind(ASTParser.K_COMPILATION_UNIT) ;
			ASTNode javaNode = javaParser.createAST(null) ;

			try {
				javaNode.accept(ClassDiagramASTVisitor.getInstance()) ;
				javaNode.accept(ControlFlowDiagramASTVisitor.getInstance()) ;
				ControlFlowDiagramXMLWriter.getInstance().buildFile("./ControlFlowOutput") ;
			} catch (TransformerException e) {
				e.printStackTrace();
			}
		}
	}

	public void fileForClassDiagram(File javaFile) throws IOException {
		System.out.println("Genarating big file...");
		String javaSource = FileUtils.readFileToString(javaFile) ;
		ASTParser javaParser = ASTParser.newParser(AST.JLS3) ;
		javaParser.setSource(javaSource.toCharArray()) ;
		javaParser.setKind(ASTParser.K_COMPILATION_UNIT) ;
		ASTNode javaNode = javaParser.createAST(null) ;

		try {
			javaNode.accept(ClassDiagramASTVisitor.getInstance()) ;
			ClassDiagramXMLWriter.getInstance().buildFile("./ClassDiagramOutput") ;
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public void unzipJar(String destinationDir, String jarPath) throws IOException {
		File file = new File(jarPath) ;
		jar = new JarFile(file) ;

		System.out.println("Writing please wait...") ;
		for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements(); ) {
			JarEntry entry = (JarEntry) enums.nextElement() ;

			String fileName = destinationDir + File.separator + entry.getName() ;
			File f = new File(fileName) ;

			if (fileName.endsWith("/")) {
				f.mkdirs() ;
			}
		}

		for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements(); ) {
			JarEntry entry = (JarEntry) enums.nextElement() ;

			String fileName = destinationDir + File.separator + entry.getName() ;
			File f = new File(fileName) ;
			if (!fileName.endsWith("/")) {
				InputStream is = jar.getInputStream(entry) ;
				FileOutputStream fos = new FileOutputStream(f) ;

				while (is.available() > 0) {
					fos.write(is.read()) ;
				}

				fos.close() ;
				is.close() ;
			}
		}
	}

	public void deCompile(String fileToDecompile, String bigFileDes) {
		System.out.println("Done writing, now decompiling please wait...");
		try {
			File file = new File (bigFileDes);
			file.getParentFile().mkdirs();
			final PrintWriter writer = new PrintWriter(file);
			List<File> files = new LinkedList<File>() ;
			Files.walk(Paths.get(fileToDecompile)).forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					File f = new File(filePath.toString());
					files.add(f) ;
					files.toString();
				}
			});

			System.out.println(files.get(10).getPath());
			System.out.println(files.get(10).getPath().matches("(.)+(class)"));
			for(int i = 0; i < files.size(); i++){
				if(files.get(i).getPath().matches("(.)+(class)")) {
					System.out.println(files.get(i));
					Decompiler.decompile(files.get(i).toString(), new PlainTextOutput(writer));
				}
			}
			writer.flush();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}