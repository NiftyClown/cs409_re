package analysis.staticc.jdt.client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GUI {

	private JFileChooser fileChooser;
	private JFrame frame ;
	private JPanel panel ;
	private JLabel label ;
	private JButton okButton ;
	private JButton cancelButton ;
	private JComboBox<String> comboBox ;

	Client client = new Client() ;

	public GUI() {
		frame = new JFrame() ;
		panel = new JPanel() ;
		label = new JLabel("Select reverse engineering task: ") ;
		okButton = new JButton("Okay") ;
		cancelButton = new JButton("Cancel") ;
		comboBox = new JComboBox<String>();
		comboBox.addItem("Class diagram") ;
		comboBox.addItem("Control flow") ;
		okButton.addActionListener(new StaticListener() );
		cancelButton.addActionListener(new StaticListener() ) ;
		comboBox.addActionListener(new StaticListener()) ;
		panel.add(label, BorderLayout.NORTH) ;
		panel.add(comboBox, BorderLayout.CENTER) ;
		panel.add(okButton, BorderLayout.SOUTH) ;
		panel.add(cancelButton, BorderLayout.SOUTH) ;
		frame.setSize(400, 100) ;
		frame.add(panel) ;
		frame.setVisible(true) ;
	}

	private void chooseFile(String choice) {
		String value=comboBox.getSelectedItem().toString();
		if(value.equals("Class diagram")) {
			fileChooser = new JFileChooser() ;
			String[] EXTENSION=new String[]{"jar","xml"};
			FileFilter fileFilter = new FileNameExtensionFilter("Files",EXTENSION) ;
			fileChooser.addChoosableFileFilter(fileFilter) ;
			fileChooser.showOpenDialog(null) ;
			File fileToUse = fileChooser.getSelectedFile() ;

			File fileUse = new File(fileToUse.toString());
			File allInOne = new File("./CompactJava/AllInOne.java");
			
			if (fileToUse != null) {
				try {
					client.unzipJar("./Resources/Extracts/", fileUse.toString());
					client.deCompile("./Resources/Extracts/", "./CompactJava/AllInOne.java");
					client.fileForClassDiagram(allInOne);
					System.out.println("Done");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else if (value.equals("Control flow")) {
			JFileChooser fileChooser = new JFileChooser();

			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			fileChooser.showOpenDialog(null);
			File file = fileChooser.getSelectedFile();
			File fileUse = new File(file.toString());
			try {
				client.readSourceForControlFlow(fileUse.toString());
				System.out.println("Done");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class StaticListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(okButton)) {
				frame.setVisible(false) ;
				chooseFile(comboBox.getSelectedItem().toString()) ;
			} else if (e.getSource().equals(cancelButton)) {
				System.exit(0) ;
			}
		}
	}
}
